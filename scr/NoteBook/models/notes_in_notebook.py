class NoteInNoteBook:

    def __init__(self, idx, name, surname, telephone_number, address, birthday):
        self.idx = idx
        self.name = name
        self.surname = surname
        self.telephone_num = telephone_number
        self.address = address
        self.birthday = birthday

    def __str__(self):
        return f"{self.idx} Name: {self.name}  Surname: {self.surname}  Telephone Number: {self.telephone_num}  Address: {self.address} Birthday: {self.birthday}"
