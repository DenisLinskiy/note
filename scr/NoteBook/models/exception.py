class MyException(Exception):
    def __init__(self, message):
        self.message = message

    @staticmethod
    def send_user(message: str):
        print(message)

