import os
import csv
from NoteBook.models.notes_in_notebook import NoteInNoteBook
from NoteBook.models.exception import MyException
import re
from typing import List
# ----------------------------------------------------------------------------------------------------------------------
#
#    __  __     ______     ______     ______
#   /\ \/\ \   /\  ___\   /\  ___\   /\  == \
#   \ \ \_\ \  \ \___  \  \ \  __\   \ \  __<
#    \ \_____\  \/\_____\  \ \_____\  \ \_\ \_\
#     \/_____/   \/_____/   \/_____/   \/_/ /_/
#
# ----------------------------------------------------------------------------------------------------------------------


class SingletonMeta(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Controller(metaclass=SingletonMeta):

    def __init__(self):
        self.file_path = '\\'.join([os.getcwd(), 'data', 'notebook.csv'])
        if not os.path.exists(self.file_path):
            self.create_file()

    def create_file(self, rows: List = None):
        with open(self.file_path, 'w') as csv_file:
            w = csv.writer(csv_file)
            w.writerow(["IDX","NAME","SURNAME","TELEPHONE","ADDRESS","BIRTHDAY"])

            if rows is not None:
                for row in rows:
                    w.writerow(row)

    def file_reader(self):
        note_list = []
        with open(self.file_path, 'r', newline='') as file:
            r = csv.reader(file, delimiter=",")
            next(r)
            for line in r:
                if not line:
                    continue
                note_list.append(NoteInNoteBook(line[0], line[1], line[2], line[3], line[4], line[5]))
        return note_list

    def note_writer(self):
        with open(self.file_path, "a") as f:
            w = csv.writer(f)
            valid = True
            while valid:
                name = input("Enter Name: ")
                if len(name) > 2:
                    valid = False
                else:
                    print("Name len might be more 2 symbols")
            valid = True
            while valid:
                surname = input("Enter SurName: ")
                if len(surname) > 2:
                    valid = False
                else:
                    print("SurName len might be more 2 symbols")
            valid = True
            while valid:
                telephone_number = input("Enter Telephone Number: ")
                if re.match("^\\d{10}$", telephone_number):
                    valid = False
                else:
                    print("Enter valid telephone number")
            address = input("Enter Address: ")
            birthday = input("Enter Birthday: ")
            w.writerow([len(self.file_reader()) + 1, name, surname, telephone_number, address, birthday])
            print("Record successful :)")

    def show_my_notes(self):
        for item in self.file_reader():
            print(item)

    def remove_my_notes(self):
        file_data = self.file_reader()
        while True:
            try:
                number_of_notes = int(input("Enter note number: "))
                idx = number_of_notes - 1
                file_data.pop(idx)
                self.create_file(file_data)
                print("Record remove :(")
                break
            except ValueError:
                print("[ERROR] Enter correct value")
            except IndexError:
                print("[ERROR] Enter correct value")
            except csv.Error:
                print("[ERROR] Enter correct value")

    def change_record(self):
        file_data = self.file_reader()
        while True:
            try:
                number_of_notes = int(input("Enter note number: "))
                idx = number_of_notes - 1
                file_data.pop(idx)
                # .....
                self.create_file(file_data)
                print("Record change :(")
                break
            except ValueError:
                print("[ERROR] Enter correct value")
            except IndexError:
                print("[ERROR] Enter correct value")
            except csv.Error:
                print("[ERROR] Enter correct value")


    def number_of_notes(self):
        notes = self.file_reader()
        print(f"You have {len(notes)} note(s)")


controller = Controller()
