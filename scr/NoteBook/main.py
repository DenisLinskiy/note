from helpers import controller

if __name__ == '__main__':

    print("My NoteBook")

    while True:
        controller.number_of_notes()

        print('0 - Exit')
        print('1 - Add Record')
        print('2 - Remove Record')
        print('3 - Change Record')
        print('4 - Search by name')
        print('5 - Search by telephone number')
        print('6 - Search by surname first letter')
        print('7 - Sort by name')
        print('8 - Sort by surname')
        print('9 - Show my notes')

        choice = input("select number to continue: ").strip()

        match choice:
            case '0':
                print("see you soon, bye :)")
                break
            case '1':
                controller.note_writer()
            case '2':
                controller.remove_my_notes()
            case '3':
                pass
            case '4':
                pass
            case '5':
                pass
            case '6':
                pass
            case '7':
                pass
            case '8':
                pass
            case '9':
                controller.show_my_notes()
            case _:
                print('Try again')
